#!/bin/bash
# Usage: normalize_bedgraph.sh BAM_FILE BEDGRAPH_FILE NORMALIZED_BEDGRAPH_FILE
# Usage example: ./normalize_bedgraph.sh alignments/bam_filtered/A673/Alignment_Post_Processing_12.filtered.sorted.bam   alignments/wig/A673/Alignment_Post_Processing_12.filtered.sorted.bedgraph    alignments/wig/A673/Alignment_Post_Processing_12.filtered.sorted.normalized.bedgraph 
# Dependencies: Samtools 

BAM=$1
BG=$2
NORM_BG=$3

numreads=`samtools view -c $BAM`
normFactor=`echo "10000000/$numreads" | bc -l`
awk '{$4=$4*a;print}' a=$normFactor $BG > $NORM_BG
