#!/usr/bin/env python
import HTSeq
import numpy
import sys

bam_reader = HTSeq.BAM_Reader( sys.argv[1] )

#import itertools
#for read in itertools.islice( bam_reader, 10):
for read in bam_reader:
    if read.aligned:
        #print read.iv.chrom, read.iv.start, read.iv.end, read.iv.strand
        pos = None
        if read.iv.strand == "+":
            pos = read.iv.start + 4
        elif read.iv.strand == "-":
            pos = read.iv.end - 5
        print "\t".join([read.iv.chrom, str(pos), str(pos + 1)])
