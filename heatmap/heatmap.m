function heatmap(matrix, order, image_file, colorscheme, mx)

%{
matrix = 'tables/k4me1_distal_heatmap_tumors.txt'
order = 'tables/distal_heatmap_order.txt'
image_file = 'tmp.pdf'
mx = 8
colorscheme = 'colormap_darkcyan'
%}

colorscheme

x = load(matrix);
[nr, nc] = size(x)
o = load(order);
prctile(x(:), 99, 1)
x(x>mx) = mx;
x = x./max(max(x));
x = x(o,:);
imagesc(x);
axis off;
load(colorscheme) % contains cmap
colormap(cmap);
sizeFactor = 2;
w = 2* sizeFactor * nc/1000;
h = sizeFactor * nr/1000;
set(gcf, 'PaperUnits','inches','PaperSize',[w,h],'PaperPosition',[0 0 w h]);
print('-dpdf','-r1200', image_file);
exit;
