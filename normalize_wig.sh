#!/bin/bash
# Usage: normalize_wig.sh BAM_FILE WIG_FILE NORMALIZED_WIG_FILE
# Usage example: ./normalize_wig.sh alignments/bam_filtered/A673/Alignment_Post_Processing_12.filtered.sorted.bam   alignments/wig/A673/Alignment_Post_Processing_12.filtered.sorted.wig    alignments/wig/A673/Alignment_Post_Processing_12.filtered.sorted.normalized.wig 
# Dependencies: Samtools 

BAM=$1
WIG=$2
NORM_WIG=$3

numreads=`samtools view -c $BAM`
normFactor=`echo "10000000/$numreads" | bc -l`
awk '{if (!($1=="#Columns:" || $1=="variableStep")) $2=$2*a;print}' a=$normFactor $WIG > $NORM_WIG
