REFERENCE="/data/aryee/pub/genomes/Homo_sapiens_assembly19/bwa/Homo_sapiens_assembly19.fasta"
BWA="module load bwa-0.6.2; bwa"
PICARD="/apps/source/picard-tools-1.84/picard-tools-1.84"
IGVTOOLS="module load aryee/igvtools; igvtools"

align_bwa_se = {
    transform("bam", "sai", "sam") {
        exec "$BWA aln $REFERENCE ${inputs} > $output.sai"
        exec "$BWA samse $REFERENCE $output.sai ${inputs} > $output.sam"
        exec "java -Xmx2g -jar $PICARD/SamFormatConverter.jar INPUT=$output.sam OUTPUT=$output.bam VALIDATION_STRINGENCY=LENIENT"
    }
}

align_bwa_pe = { 
    transform("sai","sai", "sam", "bam") {
        exec "$BWA aln -t 8 $REFERENCE $input1.gz > $output1"
        exec "$BWA aln -t 8 $REFERENCE $input2.gz > $output2"
        exec "$BWA sampe $REFERENCE $output1 $output2 $input1.gz $input2.gz > $output.sam"
        exec "java -Xmx2g -jar $PICARD/SamFormatConverter.jar INPUT=$output.sam OUTPUT=$output.bam VALIDATION_STRINGENCY=LENIENT"
    }
    forward output.bam
}


@Filter("clean")
clean = {
    exec "java -Xmx2g -jar $PICARD/CleanSam.jar INPUT=${input} OUTPUT=${output}"
}

@Filter("sort")
sort = {
    exec "java -Xmx2g -jar $PICARD/SortSam.jar INPUT=$input OUTPUT=$output SORT_ORDER=coordinate"
}

@Filter("markdups")
markdups = {
    exec "java -Xmx2g -jar $PICARD/MarkDuplicates.jar INPUT=$input OUTPUT=$output METRICS_FILE=/dev/null CREATE_INDEX=true"
}

@Transform("tdf")
bamtotdf = {
    exec "$IGVTOOLS count -e 150 -w 25 --minMapQuality 1 $input.bam $output.tdf b37"
}

fastqtobam = segment {
    align_bwa_se + clean + sort + markdups
}

fastqtobampe = segment {
    align_bwa_pe + clean + sort + markdups
}

preproc = segment {
	fastqtobam + bamtotdf
}